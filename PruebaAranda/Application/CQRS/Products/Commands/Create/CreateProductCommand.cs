﻿using Application.Common.Interfaces;
using Application.DTOs;
using Domain.Models;
using MediatR;

namespace Application.CQRS.Products.Commands.Create
{
    public class CreateProductCommand : IRequest<ResponseDto>
    {
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public int CategoryId { get; set; }
        public string img { get; set; } = default!;
    }

    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, ResponseDto>
    {
        private readonly IRepository<Product> _repository;

        public CreateProductCommandHandler(IRepository<Product> repository)
        {
            _repository = repository;
        }


        async Task<ResponseDto> IRequestHandler<CreateProductCommand, ResponseDto>
            .Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var newProduct = new Product
            {
                Name = request.Name,
                Description = request.Description,
                CategoryId = request.CategoryId,
            };

            await _repository.AddAsync(newProduct);
            await _repository.SaveChangesAsync();

            return new ResponseDto(true, newProduct.Id);
        }

    }
}
